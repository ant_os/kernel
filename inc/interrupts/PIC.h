//
// Created by antifallobst on 9/11/22.
//

#ifndef COSMOS_PIC_H
#define COSMOS_PIC_H

#define PIC1_COMMAND    0x20
#define PIC1_DATA       0x21
#define PIC2_COMMAND    0xA0
#define PIC2_DATA       0xA1
#define PIC_EOI         0x20

#define ICW1_INIT       0x10
#define ICW1_ICW4       0x01
#define ICW4_8086       0x01

typedef enum {
    // PIC1
    PIC_IRQ_PIT,
    PIC_IRQ_KEYBOARD,
    PIC_IRQ_CASCADE,
    PIC_IRQ_COM2,
    PIC_IRQ_COM1,
    PIC_IRQ_LPT2,
    PIC_IRQ_FLOPPY,
    PIC_IRQ_SPURIOUS,

    // PIC2
    PIC_IRQ_CMOS_RTC,
    PIC_IRQ_FREE_0,         // also used for legacy SCSI and NIC
    PIC_IRQ_FREE_1,         // also used for SCSI and NIC
    PIC_IRQ_FREE_2,         // also used for SCSI and NIC
    PIC_IRQ_PS2_MOUSE,
    PIC_IRQ_FPU,            // Coprocessor / Inter-processor
    PIC_IRQ_PRIMARY_ATA,
    PIC_IRQ_SECONDARY_ATA
} pic_irq_type_E;

void    pic_remap_pic       ();
void    pic_end_master      ();
void    pic_end_slave       ();
void    pic_mask_IRQ        (pic_irq_type_E irq);
void    pic_unmask_IRQ      (pic_irq_type_E irq);

#endif //COSMOS_PIC_H
