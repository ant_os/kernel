//
// Created by antifallobst on 8/26/22.
//

#ifndef COSMOS_INFO_CPU_H
#define COSMOS_INFO_CPU_H

#include <stdint.h>

typedef enum {
    CPU_FEATURE_ECX_SSE3        = 0,
    CPU_FEATURE_ECX_PCLMUL      = 1,
    CPU_FEATURE_ECX_DTES64      = 2,
    CPU_FEATURE_ECX_MONITOR     = 3,
    CPU_FEATURE_ECX_DS_CPL      = 4,
    CPU_FEATURE_ECX_VMX         = 5,
    CPU_FEATURE_ECX_SMX         = 6,
    CPU_FEATURE_ECX_EST         = 7,
    CPU_FEATURE_ECX_TM2         = 8,
    CPU_FEATURE_ECX_SSSE3       = 9,
    CPU_FEATURE_ECX_CID         = 10,
    CPU_FEATURE_ECX_SDBG        = 11,
    CPU_FEATURE_ECX_FMA         = 12,
    CPU_FEATURE_ECX_CX16        = 13,
    CPU_FEATURE_ECX_XTPR        = 14,
    CPU_FEATURE_ECX_PDCM        = 15,
    CPU_FEATURE_ECX_PCID        = 17,
    CPU_FEATURE_ECX_DCA         = 18,
    CPU_FEATURE_ECX_SSE4_1      = 19,
    CPU_FEATURE_ECX_SSE4_2      = 20,
    CPU_FEATURE_ECX_X2APIC      = 21,
    CPU_FEATURE_ECX_MOVBE       = 22,
    CPU_FEATURE_ECX_POPCNT      = 23,
    CPU_FEATURE_ECX_TSC         = 24,
    CPU_FEATURE_ECX_AES         = 25,
    CPU_FEATURE_ECX_XSAVE       = 26,
    CPU_FEATURE_ECX_OSXSAVE     = 27,
    CPU_FEATURE_ECX_AVX         = 28,
    CPU_FEATURE_ECX_F16C        = 29,
    CPU_FEATURE_ECX_RDRAND      = 30,
    CPU_FEATURE_ECX_HYPERVISOR  = 31,
} cpu_features_ecx_E;

typedef enum {
    CPU_FEATURE_EDX_FPU         = 0,
    CPU_FEATURE_EDX_VME         = 1,
    CPU_FEATURE_EDX_DE          = 2,
    CPU_FEATURE_EDX_PSE         = 3,
    CPU_FEATURE_EDX_TSC         = 4,
    CPU_FEATURE_EDX_MSR         = 5,
    CPU_FEATURE_EDX_PAE         = 6,
    CPU_FEATURE_EDX_MCE         = 7,
    CPU_FEATURE_EDX_CX8         = 8,
    CPU_FEATURE_EDX_APIC        = 9,
    CPU_FEATURE_EDX_SEP         = 11,
    CPU_FEATURE_EDX_MTRR        = 12,
    CPU_FEATURE_EDX_PGE         = 13,
    CPU_FEATURE_EDX_MCA         = 14,
    CPU_FEATURE_EDX_CMOV        = 15,
    CPU_FEATURE_EDX_PAT         = 16,
    CPU_FEATURE_EDX_PSE36       = 17,
    CPU_FEATURE_EDX_PSN         = 18,
    CPU_FEATURE_EDX_CLFLUSH     = 19,
    CPU_FEATURE_EDX_DS          = 21,
    CPU_FEATURE_EDX_ACPI        = 22,
    CPU_FEATURE_EDX_MMX         = 23,
    CPU_FEATURE_EDX_FXSR        = 24,
    CPU_FEATURE_EDX_SSE         = 25,
    CPU_FEATURE_EDX_SSE2        = 26,
    CPU_FEATURE_EDX_SS          = 27,
    CPU_FEATURE_EDX_HTT         = 28,
    CPU_FEATURE_EDX_TM          = 29,
    CPU_FEATURE_EDX_IA64        = 30,
    CPU_FEATURE_EDX_PBE         = 31
}cpu_features_edx_E;

typedef struct {
    uint64_t    largest_standard_function;
    char        vendor_string               [12];
    uint64_t    chip_id;
    uint64_t    features_ecx;
    uint64_t    features_edx;
} cpu_info_T;

void    cpu_get_information     (cpu_info_T* cpu_info);

extern cpu_info_T* g_cpu_info;

#endif //COSMOS_INFO_CPU_H
