//
// Created by antifallobst on 8/28/22.
//

#ifndef COSMOS_UHCI_H
#define COSMOS_UHCI_H

#include <drivers/PCI.h>

typedef struct {
    uint8_t                             run                             :1;
    uint8_t                             host_controller_reset           :1;
    uint8_t                             global_reset                    :1;
    uint8_t                             global_suspend                  :1;
    uint8_t                             global_resume                   :1;
    uint8_t                             software_debug                  :1;
    uint8_t                             configure                       :1;
    uint8_t                             max_packet                      :1;
    uint8_t                             reserved_0                      :8;
} uhci_command_register_T;

typedef struct {
    uint8_t                             interrupt                       :1;
    uint8_t                             error_interrupt                 :1;
    uint8_t                             resume_detected                 :1;
    uint8_t                             system_error                    :1;
    uint8_t                             process_error                   :1;
    uint8_t                             halted                          :1;
    uint16_t                            reserved_0                      :10;
} uhci_status_register_T;

typedef struct {
    uint8_t                             timeout_crc                     :1;
    uint8_t                             resume                          :1;
    uint8_t                             complete                        :1;
    uint8_t                             short_packet                    :1;
    uint16_t                            reserved_0                      :12;
} uhci_interrupt_enable_register_T;

typedef struct {
    uint8_t                             connected                       :1;
    uint8_t                             connected_changed               :1;
    uint8_t                             port_enabled                    :1;
    uint8_t                             port_enable_changed             :1;
    uint8_t                             line_status                     :2;
    uint8_t                             resume_detected                 :1;
    uint8_t                             reserved_0                      :1;
    uint8_t                             low_speed                       :1;
    uint8_t                             reset                           :1;
    uint8_t                             reserved_1                      :2;
    uint8_t                             suspend                         :1;
    uint16_t                            reserved_2                      :3;
} uhci_port_register_T;

typedef struct {
    uhci_command_register_T             usb_command;
    uhci_status_register_T              usb_status;
    uhci_interrupt_enable_register_T    usb_interrupt_enable;
    uint16_t                            frame_number;
    uint32_t                            frame_list_base_address;
    uint8_t                             start_frame;
    uhci_port_register_T                port_0;
    uhci_port_register_T                port_1;
} uhci_io_registers_T;

void uhci_init(pci_device_header_T* pci_base_address);

#endif //COSMOS_UHCI_H
