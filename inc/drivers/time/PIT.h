//
// Created by antifallobst on 8/18/22.
//

#ifndef COSMOS_PIT_H
#define COSMOS_PIT_H

#include <stdint.h>
#include <drivers/cpu.h>

#define PIT_DIVISOR 32768 // this should produce an interrupt all 27.46ms, what is perfect for preemptive multithreading

extern double   pit_time_since_boot;

void        pit_sleep_seconds       (double seconds);
void        pit_sleep_milliseconds  (uint64_t milliseconds);

void        pit_set_divisor         (uint16_t divisor);
uint64_t    pit_get_frequency       ();
void        pit_set_frequency       (uint64_t frequency);
void        pit_tick                (cpu_state_T* state);

#endif //COSMOS_PIT_H
