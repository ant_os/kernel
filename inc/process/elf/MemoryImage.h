//
// Created by antifallobst on 9/1/22.
//

#ifndef COSMOS_MEMORYIMAGE_H
#define COSMOS_MEMORYIMAGE_H

#include <stdint.h>
#include <stddef.h>
#include <utils/Symbol.h>

typedef enum {
    MAPPING_COPY,
    MAPPING_ZERO
} elf_memory_mapping_type_E;

typedef struct {
    uint64_t                    virtual_offset;
    uint64_t                    length;
    uint64_t                    offset_in_file;
    uint64_t                    length_in_file;
    uint8_t                     type;
} elf_memory_mapping_T;

typedef struct {
    symbol_T*                   entry_point;
    symbols_T                   symbols;
} elf_memory_image_T;

#endif //COSMOS_MEMORYIMAGE_H
