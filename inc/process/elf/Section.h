//
// Created by antifallobst on 8/17/22.
//

#ifndef COSMOS_SECTION_H
#define COSMOS_SECTION_H

#include <stdint.h>
#include <stddef.h>

typedef enum {
    SECTION_NULL,
    SECTION_PROGRAM_DATA,
    SECTION_SYMBOL_TABLE,
    SECTION_STRING_TABLE,
    SECTION_RELOCATION_ADDENDS,
    SECTION_HASH,
    SECTION_DYNAMIC_LINK,
    SECTION_NOTE,
    SECTION_NOBITS
} elf_section_type_E;

typedef struct {
    uint32_t        name_offset;
    uint32_t        type;
    uint64_t        flags;
    uint64_t        virtual_address;
    uint64_t        offset;
    uint64_t        length;
    uint32_t        link;
    uint32_t        info;
    uint64_t        alignment;
    uint64_t        entry_size;
} elf_section_entry_x64_T;

void elf_section_parse_table_x64 (uint8_t* buffer, size_t buffer_size, elf_section_entry_x64_T* sections, size_t num_sections);

#endif //COSMOS_SECTION_H
