//
// Created by antifallobst on 9/21/22.
//

#ifndef COSMOS_ELF_SYMBOL_H
#define COSMOS_ELF_SYMBOL_H

#include <stdint.h>

#define ELF_SYMBOL_TYPE(info)   ((info) & 0xf)

extern char* elf_symbol_types[7];

typedef enum {
    ELF_SYM_NONE,
    ELF_SYM_OBJECT,
    ELF_SYM_FUNC,
    ELF_SYM_SECTION,
    ELF_SYM_FILE,
    ELF_SYM_COMMON,
    ELF_SYM_TLS
} elf_symbol_type_E;

typedef struct {
    uint32_t    name;
    uint8_t     info;
    uint8_t     other;
    uint16_t    related_section_index;
    uint64_t    value;
    uint64_t    length;
} elf_symbol_T;

#endif //COSMOS_ELF_SYMBOL_H
