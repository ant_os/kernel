//
// Created by antifallobst on 8/17/22.
//

#ifndef COSMOS_PROGRAM_H
#define COSMOS_PROGRAM_H

#include <stdint.h>
#include <stddef.h>

#define ELF_PROGRAM_HEADER_ENTRY_SIZE_X64   0x38

typedef enum {
    ELF_PROGRAM_ENTRY_NULL                      = 0,
    ELF_PROGRAM_ENTRY_LOADABLE                  = 1,
    ELF_PROGRAM_ENTRY_DYNAMIC_LINK_INFO         = 2,
    ELF_PROGRAM_ENTRY_INTERPRETER_INFO          = 3,
    ELF_PROGRAM_ENTRY_NOTE                      = 4,
    ELF_PROGRAM_ENTRY_PROGRAM_HEADER_TABLE      = 6,
    ELF_PROGRAM_ENTRY_THREAD_LOCAL_STORAGE      = 7
} elf_segment_entry_type_E;

typedef enum {
    ELF_PROGRAM_FLAG_EXECUTABLE                 = 1,
    ELF_PROGRAM_FLAG_WRITABLE                   = 2,
    ELF_PROGRAM_FLAG_READABLE                   = 4
} elf_segment_entry_flags_E;

typedef struct {
    elf_segment_entry_type_E    type;
    uint32_t                    offset_segment;
    uint32_t                    virtual_address;
    uint32_t                    physical_address;
    uint32_t                    length_on_disk;
    uint32_t                    length_in_mem;
    uint32_t                    flags;
    uint32_t                    alignment;
} elf_program_entry_x32_T;

typedef struct {
    elf_segment_entry_type_E    type;
    uint32_t                    flags;
    uint64_t                    offset_segment;
    uint64_t                    virtual_address;
    uint64_t                    physical_address;
    uint64_t                    length_on_disk;
    uint64_t                    length_in_mem;
    uint64_t                    alignment;
} elf_program_entry_x64_T;

int     elf_program_get_loadable_segments   (elf_program_entry_x64_T* segment_table, size_t num_entries);
void    elf_program_parse_table_x64         (void *buffer, size_t buffer_size, elf_program_entry_x64_T *entries, size_t num_entries);

#endif //COSMOS_PROGRAM_H
