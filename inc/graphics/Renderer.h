//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_RENDERER_H
#define COSMOS_RENDERER_H

#include <stdarg.h>
#include <stdbool.h>
#include <graphics/Framebuffer.h>
#include <graphics/Color.h>
#include <utils/Math.h>

#define CHAR_WIDTH  8
#define CHAR_HEIGHT 16

void        printf                  (const char* str, ...);
void        put_pixel               (point_T coords, color_t clr);
color_t     get_pixel               (point_T coords);
void        scroll                  ();

#endif //COSMOS_RENDERER_H
