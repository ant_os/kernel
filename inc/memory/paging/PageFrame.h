//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_PAGEFRAME_H
#define COSMOS_PAGEFRAME_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <memory/EFIMemory.h>

void        read_EFI_memory_map     (EFI_MEMORY_DESCRIPTOR_T* memory_map, size_t memory_map_size, size_t memory_map_descriptor_size);
void        free_page               (void* address);
void        free_pages              (void* address, uint64_t page_count);
void        lock_page               (void* address);
void        lock_pages              (void* address, uint64_t page_count);
void*       request_page            ();
uint64_t    get_free_RAM            ();
uint64_t    get_used_RAM            ();
uint64_t    get_reserved_RAM        ();


#endif //COSMOS_PAGEFRAME_H
