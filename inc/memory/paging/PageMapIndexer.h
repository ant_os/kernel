//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_PAGEMAPINDEXER_H
#define COSMOS_PAGEMAPINDEXER_H

#include <stdint.h>

typedef struct {
    uint64_t    page_directory_page_index;
    uint64_t    page_directory_index;
    uint64_t    page_table_index;
    uint64_t    page_index;
} PageMapIndexer_T;

PageMapIndexer_T    new_page_map_indexer    (uint64_t virtual_address);

#endif //COSMOS_PAGEMAPINDEXER_H
