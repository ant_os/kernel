//
// Created by antifallobst on 28/07/2022.
//

#ifndef COSMOS_PAGEMAP_H
#define COSMOS_PAGEMAP_H

#include <memory/paging/Paging.h>

void                    map_memory              (paging_page_table_T* page_map, uint64_t flags, void* virtual_memory, void* physical_memory);
void                    unmap_memory            (paging_page_table_T* page_map, void* virtual_memory);
void                    load_page_map           (paging_page_table_T* page_map);
paging_page_table_T*    new_page_map            ();
//void                    clone_page_map          (paging_page_table_T* dest, paging_page_table_T* src);

#endif //COSMOS_PAGEMAP_H
