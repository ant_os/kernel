//
// Created by antifallobst on 8/26/22.
//

#include <drivers/info/cpu.h>
#include <memory/Memory.h>

extern uint64_t     asm_cpu_id_rax              (uint64_t mode);
extern uint64_t     asm_cpu_id_rbx              (uint64_t mode);
extern uint64_t     asm_cpu_id_rcx              (uint64_t mode);
extern uint64_t     asm_cpu_id_rdx              (uint64_t mode);
extern void         asm_cpu_read_vendor_string  (char* buffer);

cpu_info_T  G_cpu_info;
cpu_info_T* g_cpu_info  = &G_cpu_info;

void cpu_get_information(cpu_info_T* cpu_info) {
    asm_cpu_read_vendor_string(cpu_info->vendor_string);
    cpu_info->largest_standard_function     = asm_cpu_id_rax(0x00000000);
    cpu_info->chip_id                       = asm_cpu_id_rax(0x00000001);
    cpu_info->features_ecx                  = asm_cpu_id_rcx(0x00000001);
    cpu_info->features_edx                  = asm_cpu_id_rdx(0x00000001);
}

bool cpu_has_feature_ecx(cpu_info_T* cpu_info, cpu_features_ecx_E feature) {
    return ((cpu_info->features_ecx & (1 << feature)) > 0);
}

bool cpu_has_feature_edx(cpu_info_T* cpu_info, cpu_features_edx_E feature) {
    return ((cpu_info->features_edx & (1 << feature)) > 0);
}