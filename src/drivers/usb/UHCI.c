//
// Created by antifallobst on 8/28/22.
//

#include <drivers/usb/UHCI.h>
#include <utils/Logger.h>
#include <utils/IO.h>
#include <memory/paging/PageMap.h>

void uhci_init(pci_device_header_T* pci_base_address) {
    uint32_t port_address = (uint32_t)((pci_header_0_T*)pci_base_address)->bar_4;
    log(LOGGING_DEBUG, "UHCI Init -> I/O Registers: %d", port_address);
}