//
// Created by antifallobst on 8/16/22.
//

#include <drivers/USTAR.h>
#include <memory/Memory.h>
#include <utils/Logger.h>
#include <utils/Math.h>
#include <utils/StrUtil.h>

char    magic[5]    = "ustar";

void ustar_analyse_archive(void* archive) {
    ustar_entry_header_T* header = (ustar_entry_header_T*)archive;

    if (!ustar_verify_header(header)) {
        log(LOGGING_ERROR, "USTAR Analyser -> archive[%x] is invalid! / Header magic mismatch", (uint64_t)archive);
        return;
    }

    log(LOGGING_NONE_PRINT, "Files:");
    while (ustar_verify_header(header)) {
        log(LOGGING_NONE_PRINT, "    SIZE[%d]  BLOCKS[%d]  NAME[%s]", ustar_get_entry_size(header), ustar_get_entry_size(header) / USTAR_BLOCK_SIZE + 1, header->filename);

        size_t  file_size               = ustar_get_entry_size(header);
        size_t  file_size_in_archive    = USTAR_BLOCK_SIZE;
        if (file_size > 0) {
            file_size_in_archive        = ceil_to(file_size, USTAR_BLOCK_SIZE) + (file_size % USTAR_BLOCK_SIZE != 0 ? USTAR_BLOCK_SIZE : 0);
        }

        header = (ustar_entry_header_T*)((uint64_t)header + file_size_in_archive);
//        log(LOGGING_NONE_PRINT, "  > ARCHIVE[%d  | %x]", header, header);
    }

    log(LOGGING_NONE_PRINT, "End of Archive Reached / Header magic mismatch");
    hexdump(((uint64_t)header), 2048);
}

ustar_entry_header_T* ustar_lookup_path(void* archive, const char* path) {
    ustar_entry_header_T* header = (ustar_entry_header_T*)archive;

    if (!ustar_verify_header(header)) {
        log(LOGGING_ERROR, "USTAR -> looking up for \"%s\" -> archive[%x] is invalid!", path, (uint64_t)archive);
        return NULL;
    }

    while (ustar_verify_header(header)) {
        if (memcmp(header->filename, (uint8_t*)path, strlen(path))) {
            return header;
        }

        size_t  file_size               = ustar_get_entry_size(header);
        size_t  file_size_in_archive    = USTAR_BLOCK_SIZE;
        if (file_size > 0) {
            file_size_in_archive        = ceil_to(file_size, USTAR_BLOCK_SIZE) + (file_size % USTAR_BLOCK_SIZE != 0 ? USTAR_BLOCK_SIZE : 0);
        }

        header = (ustar_entry_header_T*)((uint64_t)header + file_size_in_archive);
    }

    log(LOGGING_ERROR, "USTAR -> looking up for \"%s\" -> path not found!", path);
    return NULL;
}

bool ustar_verify_header(ustar_entry_header_T* header) {
    return memcmp(header->indicator, magic, 5);
}

uint64_t ustar_get_entry_size(ustar_entry_header_T* header) {
    return octal_to_binary(header->size, 11);
}

bool ustar_read_file(ustar_entry_header_T* header, uint64_t offset, void* buffer, size_t n) {
    if (!ustar_verify_header(header)) {
        log(LOGGING_ERROR, "USTAR -> reading \"%s\" -> archive entry is invalid!", header->filename);
        return false;
    }
    if (header->type - '0' != USTAR_FILE) {
        log(LOGGING_ERROR, "USTAR -> reading \"%s\" -> archive entry is not a file!", header->filename);
        return false;
    }
    if (offset + n > ustar_get_entry_size(header)) {
        n = ustar_get_entry_size(header) - offset;
    }

    memcpy(buffer, (void*)((uint64_t)header + USTAR_BLOCK_SIZE + offset), n);
    return true;
}