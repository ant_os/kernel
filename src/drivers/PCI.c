//
// Created by antifallobst on 8/26/22.
//

#include <drivers/PCI.h>
#include <drivers/AHCI.h>
#include <drivers/usb/UHCI.h>
#include <memory/paging/PageMap.h>
#include <utils/Logger.h>
#include <utils/IO.h>

void pci_enumerate_function(uint64_t device_address, uint64_t function) {
    uint64_t offset = function << 12;

    uint64_t function_address = device_address + offset;
    map_memory(g_page_map, PTF_READ_WRITE, (void*)function_address, (void*)function_address);

    pci_device_header_T* pci_device_header = (pci_device_header_T*)function_address;

    if (pci_device_header->device_id == 0 || pci_device_header->device_id == 0xFFFF) {
        return;
    }


    // TODO: when porting to a module this need to get signals to the kernel, that this function was found
    switch (pci_device_header->class) {
        case PCI_C_MASS_STORAGE_CTRLR: {
            switch (pci_device_header->subclass) {
                case PCI_C_MASS_SC_SATA: {
                    switch (pci_device_header->prog_if) {
                        case 0x01:
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found SATA drive");
                            ahci_driver_init(g_ahci_driver, pci_device_header);
                            break;
                    }
                }
            }
        }

        case PCI_C_SERIAL_BUS_CTRLR: {
            switch (pci_device_header->subclass) {
                case PCI_C_SBUS_SC_USB: {
                    switch (pci_device_header->prog_if) {
                        case 0x00: {
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found UHCI Controller");
                            uhci_init(pci_device_header);
                            break;
                        }
                        case 0x10: {
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found OHCI Controller");
                            break;
                        }
                        case 0x20: {
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found EHCI Controller");
                            break;
                        }
                        case 0x30: {
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found XHCI Controller");
                            break;
                        }
                        case 0xFE: {
                            log(LOGGING_DEBUG, "PCI Enumerator -> Found USB Device");
                            break;
                        }
                    }
                }
            }
        }
    }
}

void pci_enumerate_device(uint64_t bus_address, uint64_t device) {
    uint64_t offset = device << 15;

    uint64_t device_address = bus_address + offset;
    map_memory(g_page_map, PTF_READ_WRITE, (void*)device_address, (void*)device_address);

    pci_device_header_T* pci_device_header = (pci_device_header_T*)device_address;

    if (pci_device_header->device_id == 0 || pci_device_header->device_id == 0xFFFF) {
        return;
    }

    for (uint64_t function = 0; function < 8; function++) {
        pci_enumerate_function(device_address, function);
    }
}

void pci_enumerate_bus(uint64_t base_address, uint64_t bus) {
    uint64_t offset = bus << 20;

    uint64_t bus_address = base_address + offset;
    map_memory(g_page_map, PTF_READ_WRITE, (void *)bus_address, (void*)bus_address);

    pci_device_header_T* pci_device_header = (pci_device_header_T*)bus_address;

    if (pci_device_header->device_id == 0 || pci_device_header->device_id == 0xFFFF) {
        return;
    }

    for (uint64_t device = 0; device < 32; device++) {
        pci_enumerate_device(bus_address, device);
    }
}

void pci_enumerate_pci(acpi_mcfg_header_T* mcfg) {
    int entries = ((mcfg->header.length) - sizeof(acpi_mcfg_header_T)) / sizeof(acpi_device_config_T);
    for (int i = 0; i < entries; i++) {
        acpi_device_config_T* new_device_config = &((acpi_device_config_T*)((uint64_t)mcfg + sizeof(acpi_mcfg_header_T)))[i];
        for (uint64_t bus = new_device_config->start_bus; bus < new_device_config->end_bus; bus++) {
            pci_enumerate_bus(new_device_config->base_address, bus);
        }
    }
}