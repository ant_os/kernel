
asm_cpu_read_vendor_string:
    mov rax,0
    cpuid

    mov [rdi+0], rbx
    mov [rdi+4], rdx
    mov [rdi+8], rcx

GLOBAL asm_cpu_read_vendor_string

asm_cpu_id_rax:
    mov rax, rdi
    cpuid
    ret
GLOBAL asm_cpu_id_rax

asm_cpu_id_rbx:
    mov rax, rdi
    cpuid
    mov rax, rbx
    ret
GLOBAL asm_cpu_id_rbx

asm_cpu_id_rcx:
    mov rax, rdi
    cpuid
    mov rax, rcx
    ret
GLOBAL asm_cpu_id_rcx

asm_cpu_id_rdx:
    mov rax, rdi
    cpuid
    mov rax, rdx
    ret
GLOBAL asm_cpu_id_rdx
