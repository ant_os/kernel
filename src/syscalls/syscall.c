//
// Created by antifallobst on 10/3/22.
//

#include <syscalls/syscall.h>
#include <utils/Logger.h>
#include <utils/Status.h>
#include <process/Scheduler.h>
#include <process/IPC.h>
#include <drivers/File.h>
#include <graphics/Renderer.h>
#include <kernel/Permission.h>
#include <module/Module.h>
#include <module/ModuleManager.h>
#include <module/ModCalls.h>

cpu_state_T* syscall_handle(cpu_state_T* state) {
    int             id      = (int)state->rax;
    uint64_t        arg1    =      state->rdi;
    uint64_t        arg2    =      state->rsi;
    uint64_t        arg3    =      state->rdx;
    uint64_t        arg4    =      state->rcx;
    uint64_t*       ret_val =     &state->rax;
    cpu_state_T*    ret     =      state;
    thread_T*       thread  =      g_scheduler->threads[g_scheduler->current_tid];
    process_T*      proc    =      g_scheduler->processes[thread->pid];

    switch(id) {
        case SYSCALL_MISC_EXIT: {
            log(LOGGING_WARNING, "Syscall -> PROC[%d] exited with code %d", thread->pid, arg1);

            if (thread->ret_val_ptr != NULL) {
                *thread->ret_val_ptr = arg1;
            }

            kill_process(g_scheduler, (int)thread->pid);

            cpu_state_T* new_state = NULL;

            while (new_state == NULL) {
                new_state = switch_thread(g_scheduler, state);
            }
            ret = new_state;

            break;
        }

        case SYSCALL_FS_OPEN: {
            if (!check_path((const char*)arg1)) {
                *ret_val         = STATUS_RESOURCE_NOT_AVAILABLE;
                *(uint64_t*)arg4 = 0;
                break;
            }
            if (!permission_path_access(thread, (const char*)arg1, arg3)) {
                *ret_val         = STATUS_PERMISSION_DENIED;
                *(uint64_t*)arg4 = 0;
                break;
            }

            *(uint64_t*)arg4    = process_filemgr_add_file(&proc->descriptors.file_manager, (const char*)arg1, arg3);
            *ret_val            = STATUS_SUCCESS;
            break;
        }
        case SYSCALL_FS_CLOSE: {
            if (process_filemgr_remove_file(&proc->descriptors.file_manager, arg1)) {
                *ret_val = STATUS_SUCCESS;
            } else {
                *ret_val = STATUS_ERROR;
            }
            break;
        }
        case SYSCALL_FS_CREATE: {
            log(LOGGING_WARNING, "Syscall -> fs_create N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_FS_DELETE: {
            log(LOGGING_WARNING, "Syscall -> fs_delete N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_FS_READ: {
            file_T* file = process_filemgr_get_file(&proc->descriptors.file_manager, arg1);

            if (file == NULL) {
                log(LOGGING_WARNING, "Syscall -> fs_read tried to read unknown file descriptor %d", arg1);
                *ret_val = STATUS_RESOURCE_NOT_AVAILABLE;
                break;
            }

            log(LOGGING_DEBUG, "Syscall -> fs_read DESCRIPTOR[%d]", arg1);
            read_file(file, arg2, (void*)arg3, arg4);

            *ret_val = STATUS_SUCCESS;
            break;
        }
        case SYSCALL_FS_WRITE: {
            switch(arg1) {
                case FILE_DESC_NONE: {
                    *ret_val = STATUS_ERROR;
                    log(LOGGING_WARNING, "Syscall -> fs_write to NONE file descriptor");
                    break;
                }
                case FILE_DESC_STDOUT: {
                    *ret_val = STATUS_SUCCESS;
                    printf((const char*)arg3);
                    break;
                }
                case FILE_DESC_STDLOG: {
                    *ret_val = STATUS_SUCCESS;

                    mod_manager_call_mod(g_mod_manager, MOD_LOGGER, MOD_CALL_LOGGER_LOG, thread, 1, &arg3);

                    cpu_state_T* new_state = NULL;

                    while (new_state == NULL) {
                        new_state = switch_thread(g_scheduler, state);
                    }
                    ret = new_state;

                    break;
                }
                default: {
                    *ret_val = STATUS_ERROR;
                    log(LOGGING_WARNING, "Syscall -> fs_write not handled yet for real files", arg1);
                    break;
                }
            }
            break;
        }
        case SYSCALL_FS_STAT: {
            log(LOGGING_WARNING, "Syscall -> fs_stat N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_FS_LENSTAT: {
            log(LOGGING_WARNING, "Syscall -> fs_lenstat N/A");
            *ret_val = STATUS_ERROR;
            break;
        }

        case SYSCALL_MOD_LOAD: {
            log(LOGGING_WARNING, "Syscall -> mod_load N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_LOADSTD: {
            log(LOGGING_WARNING, "Syscall -> mod_loadstd N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_UNLOAD: {
            log(LOGGING_WARNING, "Syscall -> mod_unload N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_CREATE: {
            log(LOGGING_WARNING, "Syscall -> mod_create N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_CONFIG: {
            log(LOGGING_WARNING, "Syscall -> mod_config N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_CLEANUP: {
            log(LOGGING_WARNING, "Syscall -> mod_cleanup N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_SETSTD: {
            log(LOGGING_WARNING, "Syscall -> mod_setstd N/A");
            *ret_val = STATUS_ERROR;
            break;
        }
        case SYSCALL_MOD_GET_PID: {
            if (arg1 >= g_mod_manager->num_modules) {
                *(uint32_t*)arg2 = 0;
                *ret_val = STATUS_ERROR;
                break;
            }
            log(LOGGING_DEBUG, "Requested PID of module[%d]", arg1);
            *(uint32_t*)arg2    = (uint32_t)g_mod_manager->modules[arg1].pid;
            *ret_val            = STATUS_SUCCESS;
            break;
        }

        case SYSCALL_IPC_CONNECT: {
            process_T* server = g_scheduler->processes[arg2];

            if (!permission_ipc_connect(server->master_thread, thread, arg3)) {
                *ret_val         = STATUS_PERMISSION_DENIED;
                *(uint64_t*)arg1 = IPC_STD_DESC_NONE;
                break;
            }
            if (server->descriptors.ipc_manager.waiting_for_connection != IPC_CONN_WAITING) {
                *ret_val = STATUS_RESOURCE_BUSY;

//                log(LOGGING_DEBUG, "IPC -> [%d] cannot connect. [%d] is not waiting for a connection", process->pid, server->pid);

                *(uint64_t*)arg1 = IPC_STD_DESC_NONE;
                break;
            }

            *(uint64_t*)arg1 = ipc_manager_add_connection(&proc->descriptors.ipc_manager, server->pid, arg3);
            ipc_manager_add_connection(&server->descriptors.ipc_manager, proc->pid, server->descriptors.ipc_manager.waiting_for_mode);
            ipc_link_connections(proc->descriptors.ipc_manager.cache.connections[0], server->descriptors.ipc_manager.cache.connections[0]);

            server->descriptors.ipc_manager.waiting_for_connection = IPC_CONN_CONNECTION_AVAILABLE;
            start_process(g_scheduler, (int)server->pid);

//            log(LOGGING_DEBUG, "IPC -> [%d] connected to [%d]  SERVER_WAITING[%d]", process->pid, server->pid, server->descriptors.ipc_manager.waiting_for_connection);

            *ret_val = STATUS_SUCCESS;
            break;
        }
        case SYSCALL_IPC_CLOSE: {
            if (arg1 == IPC_STD_DESC_NONE) {
                *ret_val = STATUS_RESOURCE_NOT_AVAILABLE;
                log(LOGGING_WARNING, "Syscall -> ipc_close cannot close a NONE descriptor");
                break;
            }

            // TODO: remove connection in other process
            if (ipc_manager_remove_connection(&proc->descriptors.ipc_manager, arg1)) {
                *ret_val = STATUS_SUCCESS;
            } else {
                *ret_val = STATUS_RESOURCE_NOT_AVAILABLE;
            }
            break;
        }
        case SYSCALL_IPC_CONN_WAIT: {
            if (proc->descriptors.ipc_manager.waiting_for_connection == IPC_CONN_CONNECTION_AVAILABLE) {
                proc->descriptors.ipc_manager.waiting_for_connection = IPC_CONN_NOT_WAITING;
                *(uint64_t*)arg1 = proc->descriptors.ipc_manager.cache.descriptors[0];
                *(uint64_t*)arg2 = proc->descriptors.ipc_manager.cache.connections[0]->server_pid;
//                log(LOGGING_DEBUG, "IPC -> [%d] accepted connected with [%d]  DESC[%d]", process->pid, *(uint64_t*)arg2, *(uint64_t*)arg1);
                *ret_val = STATUS_SUCCESS;
                break;
            }
            proc->descriptors.ipc_manager.waiting_for_connection = IPC_CONN_WAITING;
            proc->descriptors.ipc_manager.waiting_for_mode       = arg3;

            pause_thread(g_scheduler, (int)thread->g_tid);

//            log(LOGGING_DEBUG, "IPC -> [%d] is waiting for a connection...  MODE[%d]", process->pid, arg3);

            *ret_val = STATUS_ERROR;

            cpu_state_T* new_state = switch_thread(g_scheduler, state);
            if (new_state != NULL) {
                ret = new_state;
            }
            break;
        }
        case SYSCALL_IPC_SEND: {
            if (arg1 == IPC_STD_DESC_NONE) {
                *ret_val = STATUS_RESOURCE_NOT_AVAILABLE;
                log(LOGGING_WARNING, "Syscall -> ipc_send to NONE descriptor");
                break;
            }

            *ret_val = (uint64_t)ipc_handle_send((int)arg1, (ipc_message_T*)arg2);
            break;
        }
        case SYSCALL_IPC_RECEIVE: {
            if (arg1 == IPC_STD_DESC_NONE) {
                *ret_val = STATUS_RESOURCE_NOT_AVAILABLE;
                log(LOGGING_WARNING, "Syscall -> ipc_receive to NONE descriptor");
                break;
            }

            *ret_val = (uint64_t)ipc_handle_receive(arg1, (ipc_message_T*)arg2);
            if (*ret_val == STATUS_RESOURCE_NOT_AVAILABLE) {
                cpu_state_T *new_state = switch_thread(g_scheduler, state);
                if (new_state != NULL) {
                    ret = new_state;
                }
            }
            break;
        }

        default: {
            log(LOGGING_WARNING, "Syscall -> unknown id %d", id);
            *ret_val = STATUS_ERROR;
            break;
        }
    }

    return ret;
}