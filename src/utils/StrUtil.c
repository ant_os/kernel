//
// Created by antifallobst on 28/07/2022.
//

#include <utils/StrUtil.h>
#include <utils/Logger.h>
#include <utils/Math.h>
#include <memory/Memory.h>
#include <memory/Heap.h>

bool strcmp(const char* a, const char* b) {

    size_t size = strlen(a);
    if (size != strlen(b)) return false;

    for (int i = 0; i < size; i++) {
        if (*a != *b) return false;
        a++;
        b++;
    }
    return true;
}

size_t strlen(const char* str) {
    char* chr = (char*)str;
    size_t len = 0;
    while(chr[len] != 0) {
        ++len;
    }
    return len;
}

size_t format(char* buffer, const char* pattern, ...) {
    va_list args;
    va_start(args, pattern);
    size_t ret = vformat(buffer, pattern, args);
    va_end(args);
    return ret;
}

size_t vformat(char* buffer, const char* pattern, va_list args) {
    uint64_t length = 0;

    const char *str;
    uint64_t len;

    while (*pattern != 0) {
        if (*pattern == '%') {
            pattern++;
            switch (*pattern) {
                case 'c': {
                    if (buffer) {
                        *buffer = (char) va_arg(args, int);
                        buffer++;
                    }
                    length++;
                    break;
                }
                case 's': {
                    str = va_arg(args, const char*);
                    len = strlen(str);
                    if (buffer) {
                        memcpy(buffer, (void *) str, len);
                        buffer += len;
                    }
                    length += len;
                    break;
                }
                case 'd': {
                    char *buf = (char *) malloc(16);
                    dec_to_string(va_arg(args, uint64_t), buf);

                    len = strlen(buf);
                    if (buffer) {
                        memcpy(buffer, (void *) buf, len);
                        buffer += len;
                    }
                    length += len;
                    free(buf);
                    break;
                }
                case 'f': {
                    char *buf = (char *) malloc(16);
                    double_to_string(va_arg(args, double), 4, buf);

                    len = strlen(buf);
                    if (buffer) {
                        memcpy(buffer, (void *) buf, len);
                        buffer += len;
                    }
                    length += len;
                    free(buf);
                    break;
                }
                case 'b': {
                    uint64_t value = va_arg(args, uint64_t);
                    int var     = 1;
                    int size    = 0;
                    while (var < value) {
                        var *= 2;
                        size++;
                    }
                    char *buf = (char *) malloc(size + 1);
                    bin_to_string(value, buf);

                    len = strlen(buf);
                    if (buffer) {
                        memcpy(buffer, (void *) buf, len);
                        buffer += len;
                    }
                    length += len;
                    free(buf);
                    break;
                }
                case '?': {
                    char *buf = (char *) malloc(6);
                    bool_to_string(va_arg(args, int)>0, buf);

                    len = strlen(buf);
                    if (buffer) {
                        memcpy(buffer, (void *) buf, len);
                        buffer += len;
                    }
                    length += len;
                    free(buf);
                    break;
                }
                case 'x': {
                    char* buf;
                    pattern++;

                    uint64_t val = va_arg(args, uint64_t);

                    if (*pattern == '8') {
                        buf = malloc(3);
                        hex_to_string_x8(val, buf);
                    }
                    else if (*pattern == '1' && *(char*)((uint64_t)pattern + 1) == '6') {
                        pattern++;
                        buf = malloc(5);
                        hex_to_string_x16(val, buf);
                    }
                    else if (*pattern == '3' && *(char*)((uint64_t)pattern + 1) == '2') {
                        pattern++;
                        buf = malloc(9);
                        hex_to_string_x32(val, buf);
                    }
                    else if (*pattern == '6' && *(char*)((uint64_t)pattern + 1) == '4') {
                        pattern++;
                        buf = malloc(17);
                        hex_to_string_x64(val, buf);
                    }
                    else {
                        pattern--;
                        buf = malloc(17);
                        hex_to_string_x64(val, buf);
                    }


                    len = strlen(buf);

                    if (buffer) {
                        memcpy(buffer, (void *) buf, len);
                        buffer += len;
                    }
                    length += len;
                    free(buf);
                    break;
                }
                default: {
                    pattern--;
                    if (buffer) {
                        *buffer = *pattern;
                        buffer++;
                    }
                    length++;
                    break;
                }
            }
        } else {
            if (buffer) {
                *buffer = *pattern;
                buffer++;
            }
            length++;
        }
        pattern++;
    }

    if (buffer) {
        *buffer = '\0';
    }

    return length;
}

char* dec_to_string(int64_t value, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }
    uint8_t is_negative = 0;

    if (value < 0) {
        is_negative = 1;
        value *= -1;
        buffer[0] = '-';
    }

    uint8_t size;
    uint64_t sizeTest = value;
    while (sizeTest / 10 > 0) {
        sizeTest /= 10;
        size++;
    }

    uint8_t index = 0;
    while (value / 10 > 0) {
        uint8_t remainder = value % 10;
        value /= 10;
        buffer[is_negative + size - index] = remainder + '0';
        index++;
    }
    uint8_t remainder = value % 10;
    buffer[is_negative + size - index] = remainder + '0';
    buffer[is_negative + size + 1] = 0;
    return buffer;
}

char* bin_to_string(uint64_t value, char* buffer) {
    int var     = 1;
    int size    = 0;
    while (var < value) {
        var *= 2;
        size++;
    }
    for (int i = 0; i < size; i++) {
        buffer[i] = (value & (1 << (size - i - 1))) > 0 ? '1' : '0';
    }
    buffer[size] = '\0';

    return buffer;
}

char* bool_to_string(bool value, char* buffer) {
    if (value) {
        memcpy(buffer, "true\0", 5);
    } else {
        memcpy(buffer, "false\0", 6);
    }
    return buffer;
}

char* hex_to_string_x8(uint8_t value, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }

    uint8_t*    ptr = &value;
    uint8_t     tmp;

    tmp                         =   ((*ptr & 0xF0) >> 4);
    buffer[0]                   =   tmp + (tmp > 9 ? 55 : '0');
    tmp                         =   ((*ptr & 0x0F));
    buffer[1]                   =   tmp + (tmp > 9 ? 55 : '0');

    buffer[2] = 0;
    return buffer;
}

char* hex_to_string_x16(uint16_t value, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }

    uint16_t*   value_ptr  = &value;
    uint8_t*    ptr;
    uint8_t     tmp;

    for (uint8_t i = 0; i < 2; i++) {
        ptr                         =   ((uint8_t*)value_ptr + i);
        tmp                         =   ((*ptr & 0xF0) >> 4);
        buffer[3 - (i * 2 + 1)]     =   tmp + (tmp > 9 ? 55 : '0');
        tmp                         =   ((*ptr & 0x0F));
        buffer[3 - (i * 2)]         =   tmp + (tmp > 9 ? 55 : '0');
    }

    buffer[4] = 0;
    return buffer;
}

char* hex_to_string_x32(uint32_t value, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }

    uint32_t*   value_ptr  = &value;
    uint8_t*    ptr;
    uint8_t     tmp;

    for (uint8_t i = 0; i < 4; i++) {
        ptr                         =   ((uint8_t*)value_ptr + i);
        tmp                         =   ((*ptr & 0xF0) >> 4);
        buffer[7 - (i * 2 + 1)]     =   tmp + (tmp > 9 ? 55 : '0');
        tmp                         =   ((*ptr & 0x0F));
        buffer[7 - (i * 2)]         =   tmp + (tmp > 9 ? 55 : '0');
    }

    buffer[8] = 0;
    return buffer;
}

char* hex_to_string_x64(uint64_t value, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }

    uint64_t*   value_ptr  = &value;
    uint8_t*    ptr;
    uint8_t     tmp;

    for (uint8_t i = 0; i < 8; i++) {
        ptr                         =   ((uint8_t*)value_ptr + i);
        tmp                         =   ((*ptr & 0xF0) >> 4);
        buffer[15 - (i * 2 + 1)]    =   tmp + (tmp > 9 ? 55 : '0');
        tmp                         =   ((*ptr & 0x0F));
        buffer[15 - (i * 2)]        =   tmp + (tmp > 9 ? 55 : '0');
    }

    buffer[16] = 0;
    return buffer;
}

char* double_to_string(double value, uint8_t precision, char* buffer) {
    if (buffer == NULL) {
        return NULL;
    }

    // Convert everything in front of the floating point
    dec_to_string((int)value, buffer);
    size_t i = strlen(buffer);
    buffer[i] = '.';
    i++;

    if (precision > 0) {
        // remove value before the floating point
        value -= (int) value;

        // shift the first n numbers behind the floating point before it, where n = precision
        value = (int) (value * pow(10, precision));
        // round the value up (hacky workaround, that makes numbers with less than 4 numbers behind the floating point more precise, and everything above a bit less precise)
//        value++;
        // again convert everything in front of the floating point
        dec_to_string((int) value, &buffer[i]);
    } else {
        buffer[i] = '0';
        i++;
        buffer[i] = '\0';
    }

    return buffer;
}

uint64_t string_to_hex_x64(const char* string) {
    char*       chr     = (char*)string;
    uint64_t    ret     = 0;

    while ((*chr >= '0' && *chr <= '9')) {
        ret *= 10;
        ret += (*chr - '0');
        chr++;
    }

    return ret;
}