//
// Created by antifallobst on 9/21/22.
//

#include <process/elf/Relocation.h>
#include <process/elf/Symbol.h>
#include <utils/Logger.h>
#include <utils/StrUtil.h>
#include <memory/Memory.h>

char* elf_relocation_types[27] = {
        "NONE (No Support)",
        "64 (No Support)",
        "PC32 (No Support)",
        "GOT32 (No Support)",
        "PLT32 (No Support)",
        "COPY",
        "GLOB_DAT (No Support)",
        "JUMP_SLOT",
        "RELATIVE (Work In Progress)",
        "GOTPCREL (No Support)",
        "32 (No Support)",
        "32S (No Support)",
        "16 (No Support)",
        "PC16 (No Support)",
        "8 (No Support)",
        "PC8 (No Support)",
        "DPTMOD64 (No Support)",
        "DTPOFF64 (No Support)",
        "TPOFF64 (No Support)",
        "TLSGD (No Support)",
        "TLSLD (No Support)",
        "DTPOFF32 (No Support)",
        "GOTTPOFF (No Support)",
        "TPOFF32 (No Support)",
        "PC64 (No Support)",
        "GOTOFF64 (No Support)",
        "GOTPC32 (No Support)"
};

void elf_relocate(elf_executable_T* executable, uint64_t load_address) {

    uint8_t* buffer = (uint8_t*)load_address;

    for (int i = 0; i < executable->header.num_section_header_entries; i++) {
        elf_section_entry_x64_T section = executable->section_table[i];

        if (section.type == SECTION_RELOCATION_ADDENDS) {
            char*                   section_name            = (char*)&executable->buffer[executable->section_header_string_table.offset + section.name_offset];
            size_t                  num_relocations         = section.length / section.entry_size;
            elf_section_entry_x64_T relocation_section      = executable->section_table[section.info];
            char*                   relocation_section_name = (char*)&executable->buffer[executable->section_header_string_table.offset + relocation_section.name_offset];


            log(LOGGING_INFO, "ELF [RELOCATE] -> Found RELA section[%d] '%s' containing %d relocations in section[%d] '%s'", i, section_name, num_relocations, section.info, relocation_section_name);

            for (int j = 0; j < num_relocations; j++) {
                elf_relocation_addend_T     relocation      = ((elf_relocation_addend_T*)&executable->buffer[section.offset])[j];
                elf_symbol_T                symbol          = ((elf_symbol_T*)&executable->buffer[executable->dynamic_symbol_table.offset])[ELF_RELOCATION_SYMBOL(relocation.info)];
                char*                       symbol_name     = (char*)&executable->buffer[executable->dynamic_string_table.offset + symbol.name];
                uint8_t                     relocation_type = ELF_RELOCATION_TYPE(relocation.info);

                log(LOGGING_NONE, "        > TYPE[X86_64_%s]  SYMBOL(%s)[%s]  ADDEND[0x%x]  RELOC_ADDR[0x%x]",
                    elf_relocation_types[relocation_type], elf_symbol_types[ELF_SYMBOL_TYPE(symbol.info)], symbol_name, relocation.addend, load_address + relocation.offset);

                shared_library_T*   library;
                elf_symbol_T        lib_symbol;
                for (int lib = 0; lib < executable->num_libraries; lib++) {
                    library                             = executable->libraries[lib];
                    elf_section_entry_x64_T lib_symtab  = ((elf_executable_T*)library->executable)->symbol_table;
                    lib_symbol                          = elf_lookup_symbol_in_table((elf_executable_T*)library->executable, lib_symtab, symbol_name);
                    if (lib_symbol.name != 0) {
                        break;
                    }
                }
                if (lib_symbol.name == 0) {
                    log(LOGGING_ERROR, "ELF -> Symbol '%s' not found in libraries", symbol_name);
                    // TODO: stop executable init
                    return;
                }

                switch(relocation_type) {
                    case ELF_REL_JUMP_SLOT: {

                        *(uint64_t*)&buffer[relocation.offset] = library->start + lib_symbol.value;

                        log(LOGGING_NONE, "            > Relocation: SYM[%s]  RELOC_ADDR[0x%x]  VALUE[0x%x]", symbol_name, load_address + relocation.offset, library->start + lib_symbol.value);
                        break;
                    }
                    case ELF_REL_COPY: {

                        memcpy(&buffer[relocation.offset], &((uint8_t*)library->start)[lib_symbol.value], symbol.length);

                        log(LOGGING_NONE, "            > Relocation: RELOC_OFFSET[0x%x]  LIB_SYM_OFFSET[0x%x]  LENGTH[%d]", relocation.offset, &((uint8_t*)library->start)[lib_symbol.value], symbol.length);
                        break;
                    }
                    case ELF_REL_RELATIVE: {

                        *(uint64_t*)&buffer[relocation.offset] = (uint64_t)(load_address + relocation.addend);
                        log(LOGGING_NONE, "            > Relocation: RELOC_OFFSET[0x%x]  RELATIVE[0x%x]", relocation.offset, relocation.addend);
                        break;
                    }
                    default: {
                        log(LOGGING_WARNING, "ELF -> Relocation type is not supported!");
                    }
                }
            }
        }
    }
}