//
// Created by antifallobst on 28/07/2022.
//

#include <memory/paging/PageMapIndexer.h>

PageMapIndexer_T new_page_map_indexer(uint64_t virtual_address) {
    PageMapIndexer_T page_map_indexer;

    virtual_address                             >>= 12;
    page_map_indexer.page_index                   = virtual_address & 0x1ff;
    virtual_address                             >>= 9;
    page_map_indexer.page_table_index             = virtual_address & 0x1ff;
    virtual_address                             >>= 9;
    page_map_indexer.page_directory_index         = virtual_address & 0x1ff;
    virtual_address                             >>= 9;
    page_map_indexer.page_directory_page_index    = virtual_address & 0x1ff;

    return page_map_indexer;
}