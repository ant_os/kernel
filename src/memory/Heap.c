//
// Created by antifallobst on 29/07/2022.
//

#include <memory/Heap.h>
#include <memory/paging/PageFrame.h>
#include <memory/paging/PageMap.h>

void*                           heap_start;
void*                           heap_end;
struct HeapSegmentHeader_T*     last_header;

void combine_forward(struct HeapSegmentHeader_T* segment) {
    if (segment->next == NULL || !segment->next->free) return;

    if (segment->next == last_header) last_header = segment;

    if (segment->next->next != NULL) {
        segment->next->next->prev = segment;
    }

    segment->length     = segment->length + segment->next->length + sizeof(struct HeapSegmentHeader_T);
    segment->next       = segment->next->next;
}

void combine_backward(struct HeapSegmentHeader_T* segment) {
    if (segment->prev != NULL && segment->prev->free) {
        combine_forward(segment->prev);
    }
}

struct HeapSegmentHeader_T* split(struct HeapSegmentHeader_T* segment, size_t length) {
    if (length < 0x10) return NULL;

    int64_t split_segment_length = segment->length - length - sizeof(struct HeapSegmentHeader_T);
    if (split_segment_length < 0x10) return NULL;

    struct HeapSegmentHeader_T* new_split_header    = (struct HeapSegmentHeader_T*)((size_t)segment + length + sizeof(struct HeapSegmentHeader_T));

    if (segment != last_header) {
        segment->next->prev                         = new_split_header;
    } else {
        last_header                                 = new_split_header;
    }
    new_split_header->next                          = segment->next;
    segment->next                                   = new_split_header;
    new_split_header->prev                          = segment;
    new_split_header->length                        = split_segment_length;
    new_split_header->free                          = segment->free;
    segment->length                                 = length;

    return new_split_header;
}

void initialize_heap(void* heap_address, size_t page_count) {
    void*   position    = heap_address;
    size_t  heap_length = page_count * 0x1000;

    for (size_t i = 0; i < page_count; i++) {
        map_memory(g_page_map, PTF_READ_WRITE, position, request_page());
        position = (void*)((size_t)position + 0x1000);
    }

    heap_start                                  = heap_address;
    heap_end                                    = (void*)((size_t)heap_start + heap_length);
    struct HeapSegmentHeader_T* start_segment   = (struct HeapSegmentHeader_T*)heap_address;
    start_segment->length                       = heap_length - sizeof(struct HeapSegmentHeader_T);
    start_segment->next                         = NULL;
    start_segment->prev                         = NULL;
    start_segment->free                         = true;
    last_header                                 = start_segment;
}

void* malloc(size_t size) {
    if (heap_start == NULL) return NULL;
    if (size % 0x10 > 0) {
        size -= (size % 0x10);
        size += 0x10;
    }

    if (size == 0) return NULL;

    struct HeapSegmentHeader_T* current_segment = (struct HeapSegmentHeader_T*)heap_start;

    while (true) {
        if (current_segment->free) {
            if (current_segment->length > size) {
                split(current_segment, size);
                current_segment->free = false;
                return (void*)((uint64_t)current_segment + sizeof(struct HeapSegmentHeader_T));
            }
            if (current_segment->length == size) {
                current_segment->free = false;
                return (void*)((uint64_t)current_segment + sizeof(struct HeapSegmentHeader_T));
            }
        }
        if (current_segment->next == NULL) break;

        current_segment = (struct HeapSegmentHeader_T*)current_segment->next;
    }
    expand_heap(size);
    return malloc(size);
}

void free(void* address) {
    if (heap_start == NULL) return;

    struct HeapSegmentHeader_T* segment = (struct HeapSegmentHeader_T*)address - 1;
    segment->free = true;

    combine_forward (segment);
    combine_backward(segment);

    free_page(segment);
}

void expand_heap(size_t length) {
    if (length % 0x1000) {
        length -= length % 0x1000;
        length += 0x1000;
    }

    size_t page_count                           = length / 0x1000;
    struct HeapSegmentHeader_T* new_segment     = (struct HeapSegmentHeader_T*)heap_end;

    for (size_t i = 0; i < page_count; i++) {
        map_memory(g_page_map, PTF_READ_WRITE, heap_end, request_page());
        heap_end = (void*)((size_t)heap_end + 0x1000);
    }

    new_segment->free       = true;
    new_segment->prev       = last_header;
    last_header->next       = new_segment;
    last_header             = new_segment;
    new_segment->next       = NULL;
    new_segment->length     = length - sizeof(struct HeapSegmentHeader_T);
    combine_backward(new_segment);
}