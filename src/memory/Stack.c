//
// Created by antifallobst on 9/19/22.
//

#include <memory/Stack.h>
#include <memory/Memory.h>
#include <utils/Logger.h>
#include <utils/Symbol.h>
#include <utils/StrUtil.h>
#include <kernel/Kernel.h>
#include <process/Scheduler.h>
#include <process/elf/LibraryManager.h>

symbol_T* stack_backtrace_symbol_lookup(uint64_t address, symbols_T symbols, uint64_t symbol_address_base) {
    uint64_t idx = 0;

    for (int index = 0; index < symbols.num_funcs; index++) {
        if (symbols.funcs[index].address + symbol_address_base <= address &&
            symbols.funcs[idx].address <= symbols.funcs[index].address) {
            idx = index;
        }
    }

    if (idx != 0) {
        return &symbols.funcs[idx];
    }
    return NULL;
}

int trace(uint64_t rip) {
    symbols_T       symbols     = g_symbols;
    symbol_T*       symbol;
    uint64_t        base;

    char scope [18];
    if (rip > (uint64_t)&_kernel_start && rip < (uint64_t)&_kernel_end) {
        symbols = g_symbols;
        base    = (uint64_t)&_kernel_start;
        memcpy(scope, "[Exec] Kernel    ", 18);
    } else if (rip > g_lib_manager->base_address && rip < g_lib_manager->libraries[g_lib_manager->num_libraries - 1].end) {
        for (int i = 0; i < g_lib_manager->num_libraries; i++) {
            if (rip > g_lib_manager->libraries[i].start && rip < g_lib_manager->libraries[i].end) {
                elf_executable_T* exec = (elf_executable_T*)(g_lib_manager->libraries[i].executable);
                symbols = exec->image.symbols;
                base    = g_lib_manager->libraries[i].start;
            }
        }

        format(scope, "[Lib ] Process %d", g_scheduler->threads[g_scheduler->current_tid]->pid);
        if (g_scheduler->threads[g_scheduler->current_tid]->pid < 10) {scope[16] = ' '; scope[17] = '\0';}
    } else {
        symbols = g_scheduler->processes[g_scheduler->threads[g_scheduler->current_tid]->pid]->executable.image.symbols;
        base    = g_scheduler->processes[g_scheduler->threads[g_scheduler->current_tid]->pid]->executable.load_address;

        format(scope, "[Exec] Process %d", g_scheduler->threads[g_scheduler->current_tid]->pid);
        if (g_scheduler->threads[g_scheduler->current_tid]->pid < 10) {scope[16] = ' '; scope[17] = '\0';}
    }

    symbol = stack_backtrace_symbol_lookup(rip, symbols, base);
    if (symbol == NULL) {
        log(LOGGING_NONE_PRINT, "     %s  ->  0x%x       ->  [Not-Found]", scope, rip);
        return 0;
    }

    log(LOGGING_NONE_PRINT, "     %s  ->  0x%x+%x16  ->  %s", scope, symbol->address + base, (uint16_t)(rip - symbol->address + base), symbol->name);

    if (memcmp(symbol->name, "_start", 6) || rip == 0 || rip == (uint64_t)&thread_end_reached) {
        return 0;
    }

    return 1;
}

void stack_backtrace(cpu_state_T* state) {
    log(LOGGING_NONE_PRINT, "\nStack Backtrace (most recent call first):");
    log(LOGGING_NONE_PRINT, "    <Type> <Scope>         <Address>                    <Function Name>");

    stack_frame_T*  stack       = (stack_frame_T*)state->rbp;
    if (trace(state->rip) == 0) {
        return;
    }

    while (true) {
        if (trace(stack->rip) == 0) {
            break;
        }
        stack = (stack_frame_T *) stack->rbp;
    }
}
